resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["10.0.0.0/22"]
}

resource "yandex_vpc_security_group" "group-1" {
  network_id = yandex_vpc_network.network-1.id


  ingress {
    protocol       = "TCP"
    description    = "rule1"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 80
  }

  ingress {
    protocol       = "TCP"
    description    = "rule2"
    v4_cidr_blocks = ["0.0.0.0/0"]
    port           = 443
  }

  ingress {
    protocol       = "UDP"
    description    = "rule3"
    v4_cidr_blocks = ["10.0.0.23/32"]
    from_port      = 10000
    to_port        = 20000
  }
}